import { showAlertFromLocalStorage } from '../local_storage_alert/show_alert_from_local_storage';

export const initAdminRegisterRunner = () => {
  showAlertFromLocalStorage();
};
